1. 
SELECT customerName FROM customers WHERE country = 'Philippines';

2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';

3.
SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';

4.
SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';

5.
SELECT customerName FROM customers WHERE state IS NULL;

6.
SELECT firstName, lastName, email FROM employees WHERE lastName = 'Patterson' AND firstName = 'Steve';

7.
SELECT customerName, country, creditLimit FROM customers WHERE country NOT LIKE 'USA' AND creditLimit > 3000;

8.
SELECT customerName FROM customers WHERE customerName NOT LIKE '%a%' AND customerName NOT LIKE 'a%' AND customerName NOT LIKE '%a'; 

9.
SELECT c.customerNumber FROM customers c 
    JOIN orders o ON c.customerNumber = o.customerNumber
    WHERE comments NOT LIKE '%DHL%';

10.
SELECT * FROM productlines WHERE textDescription LIKE '%state of the art%';

11.
SELECT DISTINCT country FROM customers;

12.
SELECT DISTINCT status FROM orders;

13.
SELECT customerName, country FROM customers WHERE country = 'USA' OR country = 'France' OR country = 'Canada';

14.
SELECT e.firstName, e.lastName, o.city FROM employees e
    JOIN offices o ON e.officeCode = o.officeCode
    WHERE o.city = 'Tokyo';

15.
SELECT customerName FROM customers c
    JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber
    WHERE e.firstName = 'Leslie' AND e.lastName = 'Thompson';

16.
SELECT p.productName, c.customerName FROM customers c
    JOIN orders o ON c.customerNumber = o.customerNumber
    JOIN orderdetails od ON od.orderNumber = o.orderNumber
    JOIN products p ON p.productCode = od.productCode
    WHERE c.customerName = 'Baane Mini Imports';

17.
SELECT e.firstName, e.lastName, c.customerName, o.country FROM employees e
    JOIN customers c ON e.employeeNumber = c.salesRepEmployeeNumber
    JOIN offices o ON e.officeCode = o.officeCode
    WHERE o.country = c.country;

18.
SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

19.
SELECT productName, MAX(MSRP) FROM products

20.
SELECT COUNT(customerNumber) FROM customers WHERE country = 'UK';

21.
SELECT COUNT(p.productCode), pl.productLine FROM products P
    JOIN productlines pl ON p.productLine = pl.productLine
    WHERE pl.productLine = p.productLine;

22.
SELECT e.employeeNumber, COUNT(c.salesRepEmployeeNumber) FROM employees e
    JOIN customers c ON e.employeeNumber = c.salesRepEmployeeNumber
    WHERE e.employeeNumber = c.salesRepEmployeeNumber

23.
SELECT p.productName, p.quantityInStock FROM products P
    JOIN productlines pl ON p.productLine = pl.productLine
    WHERE quantityInStock < 1000;